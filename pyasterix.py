import sys, struct

DEBUG=True
XDEBUG=False

def debugon():
    global DEBUG
    DEBUG = True

def debugoff():
    global DEBUG
    DEBUG = False

C256=256*256

class _Buffer(object):
    # internal object to provide 'intelegent' sequential access to the data stream
    def __init__(self, data):
        self.data = bytes(data)
        self.index = 0
        self.length = len(data)

    @property
    def octet(self):
        result = self.data[self.index]
        if DEBUG:print('%4s %4s' % (self.index, result), '{:08b}'.format(result))
        self.index+=1
        return result
    uint1 = octet
    @property
    def int1(self):
        return struct.unpack('>b', bytes([self.octet]))[0]
    @property
    def uint2(self):
        return self.octet*256+self.octet
    @property
    def int2(self):
        chars = '%c%c' % (chr(self.octet),chr(self.octet))
        chars = bytes([self.octet,self.octet])
        return struct.unpack('>h', chars)[0]
    @property
    def uint3(self):
        return self.octet*C256+self.octet*256+self.octet
    @property
    def int3(self):
        topbyte = self.octet
        extra = 0
        if topbyte>=128:
            extra = 255
        chars = '%c%c%c%c' % (chr(extra),chr(topbyte),chr(self.octet),chr(self.octet))
        chars = bytes([extra,topbyte,self.octet,self.octet])
        return struct.unpack('>i', chars)[0]
    @property
    def int4(self):
        chars = '%c%c%c%c' % (chr(self.octet),chr(self.octet),chr(self.octet),chr(self.octet))
        chars = bytes([self.octet,self.octet,self.octet,self.octet])
        return struct.unpack('>i', chars)[0]
    @property
    def time(self):
        val = self.uint3
        extra = (val % 128)/128.0
        ss = val//128
        mm = ss // 60
        hh = mm // 60
        return '%02d:%02d:%09.6f' % (hh, mm%60, (ss%60)+extra)
    def enum(self, *args):
        return args[self.octet-1]
    def string(self, n):
        result = [chr(self.octet) for _ in range(n)]
        return ''.join(result)
    @property
    def sixbitchar(self):
        result = []
        for _ in (1,2):
            d = self.uint3
            result.append(c6(Bits(d, 24,19)))
            result.append(c6(Bits(d, 18,13)))
            result.append(c6(Bits(d, 12,7)))
            result.append(c6(Bits(d, 6,1)))
        return ''.join(result)
    @property
    def more(self):
        return self.index<self.length
    def section(self, mlen):
        self.section_length = self.index+mlen
    @property
    def more_section(self):
        return self.index<self.section_length
    def skip(self, count):
        for _ in range(count):
            self.octet

class _FSpec(object):
    # Internal object to hold the 7 bits of an FSpec
    def __init__(self, d1,d2,d3,d4,d5,d6,d7):
        self.bits = []
        for _ in [d1,d2,d3,d4,d5,d6,d7]:
            if _:
                self.bits.append([_]+ _.__name__.split('_')[-2:])
            else:
                self.bits.append([None, None, None])
    def __iter__(self):
        for d in self.bits:
            yield d
    def __getitem__(self, n):
        return self.bits[n]
    def __repr__(self):
        result=[]
        for c,f,d in self.bits:
            if f:
                result.append('%s/%-3s' % (f,d))
            else:
                result.append('---/---')
        return ','.join(result)

ordA1=ord('A')-1
ordP=ord('P')
ord0=ord('0')

def c6(data):
    # extract 8*6 bit characters from 6 bytes
    typ = Bits(data, 6,5)
    val = Bits(data, 4,1)
    if typ==0:
        return chr(ordA1+val)
    if typ==1:
        return chr(ordP+val)
    if typ==2:
        return ' '
    return chr(ord0+val)

def Bit(data, n):
    # Test for a bit set (1) or not (0)
    n-=1
    result = 1 if data & 1<<n else 0
    if XDEBUG:print('Bit({:b},{:d})={:d}'.format(data, n,result))
    return result

def Bits(data, s,e):
    # Extract an unsigned value from data given a start end end bit
    if s>e: # allow any order
        s,e=e,s
    s-=1 # bits need 0.. not 1..
    e-=1
    mask = (1 << (e - s + 1)) - 1
    result = (data>>s)&mask
    if XDEBUG:print('Bits({:0b},{:d} {:d})={:d} {:0b} mask {:b}'.format(data, s,e, result, result, mask))
    return result

def Bits2c(data, s,e):
    # Extract a signed value from data given a start and end bit
    return Bits(data,s,e) # XXXX fixme for 2c

def Enum(data, *args):
    # Return an enumerated string from a value
    return args[data]

def _fspec_bytes(n):
    # simple func to return the correct number of fspec bytes
    # its more difficult than it looks!
    result = 1
    while n>result*7:
        result+=1
    return result

class Category(object):
    db = {}
    def __init__(self):
        cname = self.__class__.__name__
        assert cname.startswith('CAT')
        self.number = int(cname.replace('CAT',''))
        self.name = '%03d' % self.number
        assert self.number not in self.db
        self.db[self.number] = self
        self.frnlist_dict = dict()
        frn_dict= dict()
        # look for member methods to make FSpecs
        for frn in dir(self):
            if frn.startswith('F'): # F* are main FSpec
                frn_dict.setdefault('MAIN', {})[int(frn[1:3])] = getattr(self, frn)
            elif frn.startswith('S'): # S* are sub-FSpecs
                key, rest = frn.split('_', 1)
                frn_dict.setdefault(key, {})[int(rest[1:3])] = getattr(self, frn)
        # Loop over the methods making FSecs
        for key in frn_dict:
            args = list()
            self.frnlist_dict[key] = []
            for frn in range(8*_fspec_bytes(max(frn_dict[key]))):
                args.append(frn_dict[key].get(frn+1, None))
                if len(args)==7:
                    self.frnlist_dict[key].append(_FSpec(*args)) # create the FSpec
                    args=list()
        self.FRN=[]
        self.DATAITEM=[]

    def _fspec_calls(self, flist):
        # read the fspec octets and find the method to call
        calls = []
        for f in flist:
            d = self._buff.octet
            # be explicit for speed
            if d&128:calls.append(f[0])
            if d& 64:calls.append(f[1])
            if d& 32:calls.append(f[2])
            if d& 16:calls.append(f[3])
            if d&  8:calls.append(f[4])
            if d&  4:calls.append(f[5])
            if d&  2:calls.append(f[6])
            if not d&1: # done?
                return calls

    def _read(self, buff):
        # Internal function to read the main FSpec.. calling all others
        self.kv = list()
        self._buff = buff
        self.fspec('MAIN')
        return self.kv

    def fspec(self, key):
        # call to read an FSpec and execute all methods
        if DEBUG:print('fspec', key)
        for call, frn_, data_ in self._fspec_calls(self.frnlist_dict[key]):
            if DEBUG:print(frn_, data_)
            self.FRN.append(frn_)
            self.DATAITEM.append(data_)
            call()
            self.FRN.pop(-1)
            self.DATAITEM.pop(-1)

    def fixed(self, n):
        # Call to declare a fixed length data item
        self.result('Fixed%s'%n, ' '.join(['{:08b}'.format(self._buff.octet) for _ in range(n)]))

    def length(self):
        # Call to declare a variable length data item
        n = self._buff.octet
        self.result('Length%s'%n, ' '.join(['{:08b}'.format(self._buff.octet) for _ in range(n)]))

    def extended(self):
        # Call to declare a set of extended data items
        result = []
        while True:
            d = self._buff.octet
            result.append(d)
            if not Bit(d,1): # end when bit 1 is zero
                self.result('Extended%s'%len(result), ' '.join(['{:08b}'.format(_) for _ in result]))
                return

    def repeat(self, length):
        # Call to declare a repeated item of known length
        count = self._buff.octet
        self.result('Repeat%s'%length, count)
        for _ in range(count):
            self.result('Repeat%s/%s'%(length,_), ' '.join(['{:08b}'.format(self._buff.octet) for _ in range(length)]))

    # Use these to get 1 or 2 unsigned bytes out of the buffer
    @property
    def uint1(self):
        return self._buff.uint1
    @property
    def uint2(self):
        return self._buff.uint2

    def result(self, key, val):
        # Call to add a key, value pair to the result stream
        self.kv.append(('%s]%s/%s' % ('/'.join(self.FRN), '/'.join(self.DATAITEM), key), val))
        if DEBUG:print(self.kv[-1])

    # Use these to return results
    def time_result(self, key):
        self.result(key, self._buff.time)
    def string_result(self, key, n):
        self.result(key, self._buff.string(n))
    def sixbitchar_result(self, key):
        self.result(key, self._buff.sixbitchar)

    def bit_result(self, key, d, n):
        self.result(key, Bit(d,n))
    def bits_result(self, key, d, s, e):
        self.result(key, Bits(d, s, e))

    def i4_result(self, key):
        self.result(key, self._buff.int4)
    def i4s_result(self, key, scale):
        self.result(key, self._buff.int4*scale)

    def i3_result(self, key):
        self.result(key, self._buff.int3)
    def i3s_result(self, key, scale):
        self.result(key, self._buff.int3*scale)
    def u3_result(self, key):
        self.result(key, self._buff.uint3)
    def u3s_result(self, key, scale):
        self.result(key, self._buff.uint3*scale)

    def i2_result(self, key):
        self.result(key, self._buff.int2)
    def i2s_result(self, key, scale):
        self.result(key, self._buff.int2*scale)
    def u2_result(self, key):
        self.result(key, self._buff.uint2)
    def u2s_result(self, key, scale):
        self.result(key, self._buff.uint2*scale)

    def i1_result(self, key):
        self.result(key, self._buff.int1*scale)
    def i1s_result(self, key, scale):
        self.result(key, self._buff.int1*scale)
    def u1_result(self, key):
        self.result(key, self._buff.uint1)
    def u1s_result(self, key, scale):
        self.result(key, self._buff.uint1*scale)

    def __repr__(self):
        result = [self.name]
        for key in sorted(self.frnlist_dict):
            result.append(key)
            for f in self.frnlist_dict[key]:
                result.append(str(f))
        return '\n'.join(result)

class CAT65(Category):
    def F01_010(self):
        self.u1_result('Sac')
        self.u1_result('Sic')
    def F02_000(self):
        self.u1_result('Type')
    def F03_015(self):
        self.u1_result('ServiceId')
    def F04_030(self):
        self.time_result('Time')
    def F05_020(self):
        self.u1_result('BatchNum')
    def F06_040(self):
        d = self.octet
        self.bits_result('NOGO', d,8,7)
        self.bit_result ('OVL',  d,6)
        self.bit_result ('TSV',  d,5)
        self.bits_result('PSS',  d,4,3)
        self.bit_result ('STTN', d,2)
    def F07_050(self):
        self.u1_result('SSR')
    def F13_RE(self):
        self.length()
    def F14_SP(self):
        self.length()

CAT65()

LLLSB  = 180.0/(2**25)
MHGLSB = 360.0/65536.0
TANLSB = 360.0/65536.0
ROCDLSB= 1/6.25

class CAT62(Category):
    def F01_010(self):
        self.u1_result('Sac')
        self.u1_result('Sic')
    def F03_015(self):
        self.u1_result('ServiceId')
    def F04_070(self):
        self.time_result('Time')
    def F05_105(self):
        self.i4s_result('Lat', LLLSB)
        self.i4s_result('Lon', LLLSB)
    def F06_100(self):
        self.i3s_result('X', 0.5)
        self.i3s_result('Y', 0.5)
    def F07_185(self):
        self.i2s_result('VX', 0.25)
        self.i2s_result('VY', 0.25)
    def F08_210(self):
        self.i1s_result('AX', 0.25)
        self.i1s_result('AY', 0.25)
    def F09_060(self):
        d = self.uint2
        self.result('M3A', '%04o' % Bits(d, 1,12)) # special SSR type?
        self.bit_result('V',    d,16)
        self.bit_result('G',    d,15)
        self.bit_result('M3A+', d,14)
    def F10_245(self):
        self.fixed(7)

    def F11_380(self):
        self.fspec('S380')

    def S380_F01_ADR(self):
        self.u3_result('ADR')
    def S380_F02_ID(self):
        self.sixbitchar_result('ID')
    def S380_F03_MHG(self):
        self.u2s_result('MHG', MHGLSB)
    def S380_F04_IAS(self):
        self.fixed(2)
    def S380_F05_TAS(self):
        self.u2_result('TAS')
    def S380_F06_SAL(self):
        d = self.uint2
        self.bit_result ('SAS',    d,16)
        self.bits_result('Source', d,14,15)
        self.result('Alt',    Bits2c(d, 1,13)*25.0)
    def S380_F07_FSS(self):
        d = self.uint2
        self.bit_result('MV', d, 16)
        self.bit_result('AH', d, 15)
        self.bit_result('AM', d, 14)
        self.result('Alt', Bits(d, 13,1)*.25)

    def S380_F08_TIS(self):
        self.fixed(1)
    def S380_F09_TID(self):
        self.fixed(16)
    def S380_F10_COM(self):
        d = self.uint2
        self.bits_result('COM',  d,16,14)
        self.bits_result('STAT', d,13,11)
        assert Bit(d,10)==0
        assert Bit(d,9)==0
        self.bit_result ('SSC',  d, 8)
        self.bit_result ('ARC',  d, 7)
        self.bit_result ('AIC',  d, 6)
        self.bit_result ('B1A',  d, 5)
        self.bits_result('B1B',  d, 4,1)
    def S380_F11_SAB(self):
        self.fixed(2)
    def S380_F12_ACS(self):
        self.fixed(7)
    def S380_F13_BVR(self):
        self.i2s_result('BVR', 6.25)
    def S380_F14_GVR(self):
        self.i2s_result('GVR', 6.25)

    def S380_F15_RAN(self):
        self.i2s_result('RAN', 0.01)
    def S380_F16_TAR(self):
        d = self.uint1
        self.bits_result('TI', d, 8,7)
        assert Bits(d,6,1)==0
        d = self.uint1
        self.result('RateOfTurn', Bits2c(d,8,2)*0.25)
        assert Bit(d,1)==0
    def S380_F17_TAN(self):
        self.i2s_result('TAN', TANLSB)
    def S380_F18_GSP(self):
        self.i2s_result('GSP', 0.22)
    def S380_F19_VUN(self):
        self.fixed(1)
    def S380_F20_MET(self):
        self.fixed(8)
    def S380_F21_EMC(self):
        self.fixed(1)

    def S380_F22_POS(self):
        self.fixed(6)
    def S380_F23_GAL(self):
        self.fixed(2)
    def S380_F24_PUN(self):
        self.fixed(1)
    def S380_F25_MB(self):
        count = self.uint1
        self.result('Count', count)
        for _ in range(count):
            self.result('%s' % _, ' '.join('%02x' % (self.uint1) for _ in  range(8)))
    def S380_F26_IAR(self):
        self.u2_result('IAR')
    def S380_F27_MAC(self):
        self.u2s_result('MAC', 0.008)
    def S380_F28_BPS(self):
        d = self.uint2
        assert Bits(d,16,13)==0
        self.result('BPS', Bits(d, 12,1)*.1)

    def F12_040(self):
        d = self.uint2
        self.bits_result('TrackNum',  d, 1,12)
        self.bit_result ('STTN',      d, 13)
        assert Bits(d,14,16)==0
    def F13_080(self):
        d = self.uint1
        self.bit_result ('Mon', d,8)
        self.bit_result ('SPI', d,7)
        self.bit_result ('MRH', d,6)
        self.bits_result('SRC', d,5,3)
        self.bit_result ('CNF', d,2)
        if not Bit(d,1):
            return
        d = self.uint1
        self.bit_result('SIM', d,8)
        self.bit_result('TSE', d,7)
        self.bit_result('TSB', d,6)
        self.bit_result('FPC', d,5)
        self.bit_result('AFF', d,4)
        self.bit_result('STP', d,3)
        self.bit_result('KOS', d,2)
        if not Bit(d,1):
            return
        d = self.uint1
        self.bit_result ('AMA', d,8)
        self.bits_result('MD4', d,7,6)
        self.bit_result ('ME',  d,5)
        self.bit_result ('MI',  d,4)
        self.bits_result('MD5', d,3,2)
        if not Bit(d,1):
            return
        d = self.uint1
        self.bit_result('CST', d,8)
        self.bit_result('PSR', d,7)
        self.bit_result('SSR', d,6)
        self.bit_result('MDS', d,5)
        self.bit_result('ADS', d,4)
        self.bit_result('SUC', d,3)
        self.bit_result('AAC', d,2)
        if not Bit(d,1):
            return
        d = self.uint1
        self.bits_result('SDS',  d,8,7)
        self.bits_result('EMS',  d,6,4)
        self.bit_result ('PFT',  d,3)
        self.bit_result ('FPLT', d,2)
        if not Bit(d,1):
            return
        d = self.uint1
        self.bit_result('DUPT', d,8)
        self.bit_result('DUPF', d,7)
        self.bit_result('DUPM', d,6)
        assert not Bit(d,1)

    def F14_290(self):
        self.fspec('S290')

    def S290_F01_TRK(self):
        self.fixed(1)
    def S290_F02_PSR(self):
        self.u1s_result('PSR', 0.25)
    def S290_F03_SSR(self):
        self.u1s_result('SSR', 0.25)
    def S290_F04_MDS(self):
        self.u1s_result('MDS', 0.25)
    def S290_F05_ADS(self):
        self.u1s_result('ADS', 0.25)
    def S290_F06_ES(self):
        self.u1s_result('ES',  0.25)
    def S290_F07_VDL(self):
        self.u1s_result('VDL', 0.25)

    def S290_F08_UAT(self):
        self.u1s_result('UAT', 0.25)
    def S290_F09_LOP(self):
        self.u1s_result('LOP', 0.25)
    def S290_F10_MLT(self):
        self.u1s_result('MLT', 0.25)

    def F15_200(self):
        d=self.uint1
        self.bits_result('TRANS', d,8,7)
        self.bits_result('LONG',  d,6,5)
        self.bits_result('VERT',  d,4,3)
        self.bit_result ('ADF',   d,2)

    def F16_295(self):
        self.fspec('S295')

    def S295_F01_MFL(self):
        self.u1s_result('MFL', 0.25)
    def S295_F02_MD1(self):
        self.u1s_result('MD1', 0.25)
    def S295_F03_MD2(self):
        self.u1s_result('MD2', 0.25)
    def S295_F04_MDA(self):
        self.u1s_result('MDA', 0.25)
    def S295_F05_MD4(self):
        self.u1s_result('MD4', 0.25)
    def S295_F06_MD5(self):
        self.u1s_result('MD5', 0.25)
    def S295_F07_MHG(self):
        self.u1s_result('MHG', 0.25)

    def S295_F08_IAS(self):
        self.u1s_result('IAS', 0.25)
    def S295_F09_TAS(self):
        self.u1s_result('TAS', 0.25)
    def S295_F10_SAL(self):
        self.u1s_result('SAL', 0.25)
    def S295_F11_FSS(self):
        self.u1s_result('FSS', 0.25)
    def S295_F12_TID(self):
        self.u1s_result('TID', 0.25)
    def S295_F13_COM(self):
        self.u1s_result('COM', 0.25)
    def S295_F14_SAB(self):
        self.u1s_result('SAB', 0.25)

    def S295_F15_ACS(self):
        self.u1s_result('ACS', 0.25)
    def S295_F16_BVR(self):
        self.u1s_result('BVR', 0.25)
    def S295_F17_GVR(self):
        self.u1s_result('GVR', 0.25)
    def S295_F18_RAN(self):
        self.u1s_result('RAN', 0.25)
    def S295_F19_TAR(self):
        self.u1s_result('TAR', 0.25)
    def S295_F20_TAN(self):
        self.u1s_result('TAN', 0.25)
    def S295_F21_GSP(self):
        self.u1s_result('GSP', 0.25)

    def S295_F22_VUN(self):
        self.u1s_result('VUN', 0.25)
    def S295_F23_MET(self):
        self.u1s_result('MET', 0.25)
    def S295_F24_EMC(self):
        self.u1s_result('EMC', 0.25)
    def S295_F25_POS(self):
        self.u1s_result('POS', 0.25)
    def S295_F26_GAL(self):
        self.u1s_result('GAL', 0.25)
    def S295_F27_PUN(self):
        self.u1s_result('PUN', 0.25)
    def S295_F28_MB(self):
        self.u1s_result('MB',  0.25)

    def S295_F29_IAR(self):
        self.u1s_result('IAR', 0.25)
    def S295_F30_MAC(self):
        self.u1s_result('MAC', 0.25)
    def S295_F31_BPS(self):
        self.u1s_result('BPS', 0.25)

    def F17_136(self):
        self.i2s_result('MFL', 0.25)
    def F18_130(self):
        self.fixed(2)
    def F19_135(self):
        d = self.uint2
        self.bit_result('QNH', d,16)
        self.result('CTL', Bits2c(d,15,1)*.25)
    def F20_220(self):
        self.i2s_result('ROCD', ROCDLSB)

    def F21_390(self):
        self.fspec('S390')

    def S390_F01_TAG(self):
        self.fixed(2)
    def S390_F02_CSN(self):
        self.string_result('CSN', 7)
    def S390_F03_IFI(self):
        self.fixed(4)
    def S390_F04_FCT(self):
        d = self.uint1
        self.bits_result('GAT-OAT', d,7,8)
        self.bits_result('FR1-FR2', d,5,6)
        self.bits_result('RVSM',    d,3,4)
        self.bit_result ('HPR',     d,2)
    def S390_F05_TAC(self):
        self.fixed(4)
    def S390_F06_WTC(self):
        self.fixed(1)
    def S390_F07_DEP(self):
        self.string_result('DEP', 4)

    def S390_F08_DST(self):
        self.string_result('DST', 4)
    def S390_F09_RDS(self):
        self.fixed(3)
    def S390_F10_CFL(self):
        self.fixed(2)
    def S390_F11_CTL(self):
        self.u1_result('Centre')
        self.u1_result('Position')
    def S390_F12_TOD(self):
        self.repeat(4)
    def S390_F13_AST(self):
        self.string_result('AST', 6)
    def S390_F14_STS(self):
        d = self.uint1
        self.bits_result('EMP', d,7,8)
        self.bits_result('AVL', d,5,6)
    def S390_F15_STD(self):
        self.fixed(7)
    def S390_F16_STA(self):
        self.string_result('STA', 7)
    def S390_F17_PEM(self):
        d = self.uint2
        assert Bits(d,16,14)==0
        self.bit_result('VA', d, 13)
        self.result('M3A', '%04o' % Bits(d, 1,12)) # XXX SSR
    def S390_F18_PEC(self):
        self.fixed(7)

    def F22_270(self):
        self.extended()
    def F23_300(self):
        self.fixed(1)

    def F24_110(self):
        self.fspec('S110')

    def S110_F01_SUM(self):
        self.fixed(1)
    def S110_F02_PMN(self):
        self.fixed(4)
    def S110_F03_POS(self):
        self.fixed(6)
    def S110_F04_GA(self):
        self.fixed(2)
    def S110_F05_EM1(self):
        self.fixed(2)
    def S110_F06_TOS(self):
        self.fixed(1)
    def S110_F07_XP(self):
        self.fixed(1)

    def F25_120(self):
        self.fixed(2)
    def F26_510(self):
        count = 1
        while True:
            self.u1_result('SUI%s'%count)
            d = self.uint2
            self.bits_result('STN%s'%count, d,16,2)
            if not Bit(d,1):
                return
            count += 1

    def F27_500(self):
       self.fspec('S500')

    def S500_F01_APC(self):
        self.u2s_result('X', 0.5)
        self.u2s_result('Y', 0.5)
    def S500_F02_COV(self):
        self.u2s_result('COV', 0.5)
    def S500_F03_APW(self):
        self.fixed(4)
    def S500_F04_AGA(self):
        self.fixed(1)
    def S500_F05_ABA(self):
        self.u1s_result('ABA', 0.25)
    def S500_F06_ATV(self):
        self.u1s_result('X', 0.25)
        self.u1s_result('Y', 0.25)
    def S500_F07_AA(self):
        self.fixed(2)

    def S500_F08_ARC(self):
        self.u1s_result('ARC', 6.25)

    def F28_340(self):
        self.fspec('S340')

    def S340_F01_SID(self):
        self.fixed(2)
    def S340_F02_POS(self):
        self.fixed(4)
    def S340_F03_HEI(self):
        self.fixed(2)
    def S340_F04_MDC(self):
        self.fixed(2)
    def S340_F05_MDA(self):
        self.fixed(2)
    def S340_F06_TYP(self):
        self.fixed(1)

    def F34_RE(self):
        self.u1_result('Length')
        self.fspec('SRE')

    def SRE_F01_CST(self):
        count = self.uint1
        self.result('Count', count)
        for r in range(count):
            self.u1_result('%s/Sac' % r)
            self.u1_result('%s/Sic' % r)
            self.bits_result('%s/Typ' % r, self.uint1,1,4)
            self.u2_result('%s/LocalTrackNum' % r)

    def SRE_F02_CSN(self):
        count = self.uint1
        self.result('Count', count)
        for r in range(count):
            self.u1_result('%s/Sac' % r)
            self.u1_result('%s/Sic' % r)
            self.bits_result('%s/Typ' % r, self.uint1,1,4)

    def SRE_F03_TVS(self):
        self.i2s_result('Vx', 0.25)
        self.i2s_result('Vy', 0.25)

    def SRE_F04_STS(self):
        while True:
            d = self.uint1
            self.result('FDR', d)
            if Bit(d,1)==0:
                break

    def F35_SP(self):
        self.u1_result('Length')
        self.fspec('SSP')

    def SSP_F01_CCR(self):
        self.string_result('RC', 2)

CAT62()

class CAT63(Category):
    def F01_010(self):
        self.u1_result('Sac')
        self.u1_result('Sic')
    def F02_015(self):
        self.u1_result('ServiceId')
    def F03_030(self):
        self.time_result('Time')
    def F04_050(self):
        self.u1_result('Sac')
        self.u1_result('Sic')
    def F05_060(self):
        d = self.uint1
        self.bits_result('CON', d,7,8)
        self.bit_result ('PSR', d,6)
        self.bit_result ('SSR', d,5)
        self.bit_result ('MDS', d,4)
        self.bit_result ('ADS', d,3)
        self.bit_result ('MLT', d,2)
        if Bit(d,1)==0:
            return
        d = self.uint1
        self.bit_result('OPS', d,8)
        self.bit_result('ODP', d,7)
        self.bit_result('OXT', d,6)
        self.bit_result('MSC', d,5)
        self.bit_result('TSV', d,4)
        self.bit_result('NPW', d,3)
        self.bit_result('---', d,2)
        assert Bit(d,1)==0
    def F06_070(self):
        self.fixed(2)
    def F07_080(self):
        self.fixed(4)
    def F08_081(self):
        self.fixed(2)
    def F09_090(self):
        self.fixed(4)
    def F10_091(self):
        self.fixed(2)
    def F11_092(self):
        self.fixed(2)

    def F13_RE(self):
        self.length()
    def F14_SP(self):
        self.length()

CAT63()

def decode(data):

    buff = _Buffer(data)
    messages = []

    while buff.more:
        cat  = buff.uint1
        mlen = buff.uint2
        if DEBUG:print('CAT=%s length=%s' % (cat, mlen))

        if cat not in Category.db:
            print('unhandled CAT', cat, file=sys.stderr)
            messages.append((cat,[(']key', 'val')]))
            buff.skip(mlen-3)
            continue

        buff.section(mlen-3)
        while buff.more_section:
            if DEBUG:print('CAT %s' % cat)
            messages.append((cat, Category.db[cat]._read(buff)))

    return messages